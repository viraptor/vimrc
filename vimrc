execute pathogen#infect()

syntax on

filetype plugin on
filetype indent on

color ron
hi CursorLine   cterm=NONE ctermbg=234 ctermfg=NONE

set textwidth=80
set colorcolumn=+1
hi ColorColumn ctermbg=234

set scrolloff=7
set ruler

set backspace=eol,start,indent
set whichwrap+=<,>,h,l

set ignorecase
set smartcase

set hlsearch

set showmatch
set matchtime=2

set laststatus=2

setlocal cursorline
autocmd WinEnter * setlocal cursorline
autocmd WinLeave * setlocal nocursorline

set wildmenu
set wildmode=list:longest,list:full
set wildignore=.git,*.swp,*.swo,*/tmp/*,*.pyc,*.pyo,*~

set noerrorbells
set novisualbell
set t_vb=
set tm=500

set shiftwidth=4
set tabstop=4
set autoindent
set smartindent

map j gj
map k gk

map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

let g:airline#extensions#tabline#enabled = 1

augroup Binary
  au!
  au BufReadPre  *.qcow2 let &bin=1
  au BufReadPost *.qcow2 if &bin | %!xxd
  au BufReadPost *.qcow2 set ft=xxd | endif
  au BufWritePre *.qcow2 if &bin | %!xxd -r
  au BufWritePre *.qcow2 endif
  au BufWritePost *.qcow2 if &bin | %!xxd
  au BufWritePost *.qcow2 set nomod | endif
augroup END
